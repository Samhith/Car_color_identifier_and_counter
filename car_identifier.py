import cv2
import numpy as np
import argparse
import os
cascade_src = 'cars.xml'
car_cascade = cv2.CascadeClassifier(cascade_src)
def findcolor(x):
	if(x==0):
		return("white")
	if(x==1):
		return("black")
	if(x==2):
		return("grey")
	if(x==3):
		return("yellow")
	if(x==4):
		return("red")
	if(x==5):
		return("green")
	if(x==6):
		return("blue")
	if(x==7):
		return("brown")
	if(x==8):
		return("other")

def identify_rgb(x):
	if(abs(int(x[0])-int(x[1]))<12 and abs(int(x[1])-int(x[2]))<12):
		if(int(x[0])>205 and int(x[1])>205 and int(x[2])>205):
			return 0 #white a 
		if(int(x[0])<50 and int(x[1])<50 and int(x[2])<50):
			return 1 #black aa
		return 2 #grey
	if(abs(int(x[0])-int(x[1]))<15 and abs(int(x[1])-int(x[2]))>40):
		return 3 #yellow aaa
	
	if(x[0]>x[1] and x[0]>x[2]):
		if(abs(int(x[0]/2)-int(x[1]))<10):
			return 7 #brown aaaa
		return 4 #red aaaaa
	if(x[1]>x[0] and x[1]>x[2]):
		return 5 #green aaaaaa
	if(x[2]>x[1] and x[2]>x[0]):
		return 6 #blue aaaaaaa
	return 8 #other
	
def get_car_color(image):
    color1=[0,0,0,0,0,0,0,0,0]
    pro=[0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]
    l=len(image)
    m=len(image[0])
    for i in range(int(l*0.2),int(l*0.8)):
        for j in range(int(m*0.2),int(m*0.8)):
            k=identify_rgb(image[i][j])
            color1[k]+=1
    for i in range(8):
        pro[i]=color1[i]/(l*l)
    return color1
boundaries = [
	([17, 15, 100], [50, 56, 200]),
	([86, 31, 4], [220, 88, 50]),
	([25, 146, 190], [62, 174, 250]),
	([103, 86, 65], [145, 133, 128])
]

img=cv2.imread('car.jpg')

#img = cv2.resize(img, (700,1000))
blur = cv2.GaussianBlur(img,(5,5),0)
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)


cars = car_cascade.detectMultiScale(gray, 1.08, 1)

fp=open("car.txt","a")
for (x,y,w,h) in cars:
	cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,255),2)  
	crop_image=img[y:y+h, x:x+w]
	colors=get_car_color(crop_image)
	colors[6]=colors[6]-600
	colors[1]=colors[1]-400
	fp.write(findcolor(colors.index(max(colors))))
	fp.write(" ")
	
	
fp.close()
cv2.imshow('video', img)
cv2.waitKey(0)
cv2.destroyAllWindows()
    

